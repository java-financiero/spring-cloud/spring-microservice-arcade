package es.cloudtf.pcfutbol.pcftubolarcade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("PCFUTBOL-MARKET")
public interface Caller {

    @GetMapping("/saludo")
    public String consumeSaludo();

}
