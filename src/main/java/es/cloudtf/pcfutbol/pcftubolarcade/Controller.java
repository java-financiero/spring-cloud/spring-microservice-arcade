package es.cloudtf.pcfutbol.pcftubolarcade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    Caller caller;

    @GetMapping("/saludo")
    public String saludo(){

        String saludoDesdeMarket = caller.consumeSaludo();
        return "Soy micro arcade " + "("+saludoDesdeMarket+")";

    }


}
